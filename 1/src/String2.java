public class String2 {

    //https://codingbat.com/prob/p147448
    //Return the number of times that the string "hi" appears anywhere in the given string.

    public static void main(String[] args) {

        int d = (int) countHi("hi1 hi");
        System.out.println(d);

    }

    public static int countHi(String str){
      //  String str = "hi1 hi";
        // int nachindex = 0;
        int count = 0;
        int nachindex = str.indexOf("hi");
        // s.toLowerCase();
        while (nachindex != -1) {
            count++;
            str = str.substring(nachindex + 2);
            nachindex = str.indexOf("hi");

        }
        //    boolean d = str.contains("hi");
      //  System.out.println(count);
        return count;
    }
}

