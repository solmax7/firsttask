import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class FirstTask {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите номер задачи!!!");
        String s = reader.readLine();

        switch (s) {
            case "1": firsttask();
                ;
                break;
            case "2":
                twotask();
                break;
            case "3":
                threetask();
                break;
            case "4":
                fourtask();
                break;
            case "5":
                fivetask();
                break;
            case "6":
                sixtask();
                break;
            case "7":
                seventask();
                break;
            case "8":
                fivetask();
                break;
            default:
                System.out.println("Не выбрана задача. Перезапустите!!!");
        }
    }

    public static void firsttask(){

        // Given a list of strings, return a list where each string is replaced by 3 copies of the string concatenated together.
        //https://codingbat.com/prob/p181634

        List<String> list = new ArrayList();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        list.add("dddd");
        list.add("ee");

        for (int i = 0; i < list.size(); i++){

            String d = list.get(i);

            list.set(i , d + d + d);

            System.out.println(list.get(i));
        }

    }

    public static void twotask(){

        //We'll say that a lowercase 'g' in a string is "happy" if there is another 'g' immediately to its left or right. Return true if all the g's in the given string are happy
        //https://codingbat.com/prob/p198664

        List<String> list = new ArrayList<>();
        list.add("xxggxx");
        list.add("xxgxx");
        list.add("xxggyyggxgg");
        //   String s = "xxgggxx";

        // char[] cArray = s.toCharArray();
        //   list.add("xxgggxx");
        //   list.add("xxggyygxx");
        for (int j = 0; j < list.size(); j++) {


            String[] res = new String[list.get(j).length()];
            boolean flag = false;
            for (int i = 0; i < res.length; i++) {

                char c = list.get(j).charAt(i);
                if ((c == 'g') && (i != res.length -1)) {
                    if (list.get(j).charAt(i + 1) == 'g') {

                        flag = true;
                        i++;
                        //   System.out.println(s + " : true");
                    } else {
                        flag = false;
                        break;
                        // System.out.println(s + " : false");
                    }
                }


            }
            if (flag)
                System.out.println(list.get(j) + " : true");
            else
                System.out.println(list.get(j) + " : false");

        }
    }

    public static void threetask(){

//Return the number of times that the string "hi" appears anywhere in the given string.
//
//
//countHi("abc hi ho") → 1
//countHi("ABChi hi") → 2
//countHi("hihi") → 2

        String s = "hi1 hi";
        // int nachindex = 0;
        int count = 0;
        int nachindex = s.indexOf("hi");
        // s.toLowerCase();
        while(nachindex!=-1){
            count++;
            s = s.substring(nachindex+2);
            nachindex = s.indexOf("hi");

        }
        //    boolean d = s.contains("hi");
        System.out.println(count);

    }

    public static void fourtask(){
//        Given an array of scores, return true if each score is equal or greater than the one before. The array will be length 2 or more.
//
//        scoresIncreasing([1, 3, 4]) → true
//        scoresIncreasing([1, 3, 2]) → false
//        scoresIncreasing([1, 1, 4]) → true



        int[] scoresIncreasing = {1, 1, 4};

        int first = scoresIncreasing[0];

        boolean flag = true;

        for (int i = 0; i < scoresIncreasing.length; i++) {

            if (scoresIncreasing[i] < first){

                flag = false;
                break;

            }

            first = scoresIncreasing[i];

        }

        System.out.println(flag);

    }

    public static void fivetask(){

//        Given an array of strings, return a Map<String, Integer> containing a key for every different string in the array, always with the value 0. For example the string "hello" makes the pair "hello":0. We'll do more complicated counting later, but for this problem the value is simply 0.
//
//        https://codingbat.com/prob/p152303

//        word0(["a", "b", "a", "b"]) → {"a": 0, "b": 0}
//        word0(["a", "b", "a", "c", "b"]) → {"a": 0, "b": 0, "c": 0}
//        word0(["c", "b", "a"]) → {"a": 0, "b": 0, "c": 0}

        ArrayList <String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i < list.size(); i++) {



            map.put(list.get(i), 0);

        }

        for (String key : map.keySet()) {
            System.out.println(key + ": " +  map.get(key));
        }

    }

    public static void sixtask(){


        // https://codingbat.com/prob/p190862
//        Given an array of strings, return a Map<String, Boolean> where each different string is a key and its value is true if that string appears 2 or more times in the array.
////
//        wordMultiple(["a", "b", "a", "c", "b"]) → {"a": true, "b": true, "c": false}
//        wordMultiple(["c", "b", "a"]) → {"a": false, "b": false, "c": false}
//        wordMultiple(["c", "c", "c", "c"]) → {"c": true}

        ArrayList<String> list = new ArrayList<>();

        list.add("c");
        list.add("c");
        list.add("c");
        list.add("d");
        list.add("c");
        list.add("c");

        Map <String, Boolean> map = new HashMap<>();

        // Map <String, Integer> map1 = new HashMap<>();

        for (int i = 0; i < list.size(); i++) {

            if (!map.containsKey(list.get(i)))
                map.put(list.get(i), false);

            else{

                map.put(list.get(i), true);

            }

        }

        for (String key : map.keySet()) {
            System.out.println(key + ": " +  map.get(key));
        }

    }

    public static void seventask(){

//        You have a red lottery ticket showing ints a, b, and c, each of which is 0, 1, or 2. If they are all the value 2, the result is 10. Otherwise if they are all the same, the result is 5. Otherwise so long as both b and c are different from a, the result is 1. Otherwise the result is 0.
//
//         https://codingbat.com/prob/p170833
//        redTicket(2, 2, 2) → 10
//        redTicket(2, 2, 1) → 0
//        redTicket(0, 0, 0) → 5

        int a = 0;
        int b = 0;
        int c = 0;

        if (a == 2 && b == 2 && c == 2){

            System.out.println(10);

        }

        else if (a == b && b ==c){

            System.out.println(5);

        }

        else if (b == c && c != a){

            System.out.println(1);
        }
        else
            System.out.println(0);

    }

    public static void eighttask(){

//        Given a list of integers, return a list of those numbers, omitting any that are between 13 and 19 inclusive.
//
//          https://codingbat.com/prob/p137274
//            noTeen([12, 13, 19, 20]) → [12, 20]
//        noTeen([1, 14, 1]) → [1, 1]
//        noTeen([15]) → []

        int[] massiv = {12, 13, 19, 20};

        List<Integer> newmassiv = new ArrayList<>();



        for (int i = 0; i <massiv.length ; i++) {

            if (!(massiv[i]>=13 && massiv[i] <= 19)){

                newmassiv.add(massiv[i]);
            }

        }

    }


}

